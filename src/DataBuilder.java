/*Olio-ohjelmointi*/
/*Harjoitustyö*/

/*Johannes Kohvakka*/
/*0418569*/
/*Aki Hyvönen*/
/*0438400*/


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;



/*DataBuilder parses .xml gotten from URL and creates SmartPost objects from the data*/

public class DataBuilder {
    
    
    private Document doc;
    private HashMap<String, Smartpost> map;
    
    public HashMap<String, Smartpost> getMap() {
        return map;
    }
    private ArrayList<String> cities;
    
    
    /*Constructor reads .xml-file and passes it to function parseCurrentData*/
        
    
    public DataBuilder() throws MalformedURLException, IOException, ParserConfigurationException, SAXException{
        
        
        cities = new ArrayList();                   
        String content = "";
        String line;
        
            
        URL url = new URL("http://smartpost.ee/fi_apt.xml?mode=xml");

        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

        while((line = br.readLine()) != null){
            content += line + "\n";
        }

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

        doc = dBuilder.parse(new InputSource(new StringReader(content)));

        doc.getDocumentElement().normalize();

        map = new HashMap();

        parseCurrentData();
            
        }     
    
    
    /*Function parses Document created in constructor*/
    /*And creates SmartPost objects from the information*/
    /*And puts smartposts to HashMap*/
    private void parseCurrentData(){
       
        NodeList nodes = doc.getElementsByTagName("place");
        
        
        for(int i = 0; i<nodes.getLength(); i++){
            Node node = nodes.item(i);
            Element e = (Element) node;       
            
            map.put(getValue("postoffice",e), new Smartpost(getValue("code", e), getValue("city",e), getValue("address",e), getValue("availability",e), getValue("postoffice",e), getValue("lat",e), getValue("lng",e)));
            //System.out.println(getValue("Name",e));
            
            if(!cities.contains(getValue("city",e))) {
                cities.add(getValue("city",e));
                
            }
            
        }
       
    }
    
    private String getValue(String tag, Element e){
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();    
    }
    
    
    public ArrayList<String> getCities() {
        
        return cities;
    }
    
    
    /*Function which returns all the SmartPosts created*/
    public ArrayList<Smartpost> getPosts(String city) {
        ArrayList<Smartpost> posts = new ArrayList();
        
        for(Entry<String,Smartpost> entry : map.entrySet()) {
            
            if(entry.getValue().getCity().equals(city)){
                posts.add(entry.getValue());
                System.out.println(entry.getValue());
            }
                
            
        }
        
        return posts;
    }
    
}

