/*Olio-ohjelmointi*/
/*Harjoitustyö*/

/*Johannes Kohvakka*/
/*0418569*/
/*Aki Hyvönen*/
/*0438400*/


/**
 *packages contains objects that have attributes about weight, size and if they are breakable or not
 * 
 */
public class Object {
    private String name;
    private String size;
    private double mass;
    private boolean breakable;
    
    public Object(String n, String s, double m, boolean b) {
        
        name = n;
        size = s;
        mass = m;
        breakable = b;
    }

    /**
     * @return the name
     */
    @Override
    public String toString() {
        return name;
    }
    
    public String getName() {
        return name;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @return the mass
     */
    public double getMass() {
        return mass;
    }

    boolean getBreakable() {
        return breakable;
    }
    
}
