/*Olio-ohjelmointi*/
/*Harjoitustyö*/

/*Johannes Kohvakka*/
/*0418569*/
/*Aki Hyvönen*/
/*0438400*/


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


/*Opens up windows and creates log file when window is closed */

/**
 *
 * @author JOZ
 */


/*Opens up windows and creates log file when window is closed */
public class Mainclass extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        scene.getStylesheets().add
        (FXMLDocumentController.class.getResource("newCascadeStyleSheet.css").toExternalForm());
        stage.show();
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
        
        //when window is closed writes log file
        Storage storage = Storage.getInstance();
        
        try {
            BufferedWriter br = new BufferedWriter(new FileWriter("log.txt"));
            if(storage.getLog().isEmpty()) {
                br.write("Paketteja lähetetty ei!");
            } else {
                br.write("Kirjanpito lähetetyistä paketeista:\n");
                br.write("***************************************************************\n");
                br.write(storage.getLog());
            }
            br.close();
        } catch (IOException e) {
            
        }
    }
    
}
