/*Olio-ohjelmointi*/
/*Harjoitustyö*/

/*Johannes Kohvakka*/
/*0418569*/
/*Aki Hyvönen*/
/*0438400*/



import java.util.ArrayList;


/*SmartPost contains information about location of current post (coordinated, address)*/
/*Class got getters to each attribute and getter to GeoPoint which contains coordinates*/
public class Smartpost {
    
    ArrayList<String> GeoList =new ArrayList();
    protected String postcode;
    protected String city;
    protected String address;
    protected String availability;
    protected String postoffice;
    protected String latitude;
    protected String longitude;
            
            
    public Smartpost(String pc, String ct, String adr, String avail, String po, String lat, String lng){
        
        postcode = pc;
        city = ct;
        address = adr;
        availability = avail;
        postoffice = po;
        latitude = lat;
        longitude = lng;
        
    }

    /**
     * @return the postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @return the availability
     */
    public String getAvailability() {
        return availability;
    }

    /**
     * @return the postoffice
     */
    
    @Override
    public String toString() {
       return postoffice;
    }

    /**
     * @return the latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * @return the longitude
     */
    public String getLongitude() {
        return longitude;
    }
    
    public String getLocation(){
        
        return ("'"+address+", "+postcode+" "+city+"', '"+postoffice+" avoinna: "+availability+"', 'red'");
    
    }
    
    public ArrayList GeoPoint(){
        
        GeoList.add(latitude);
        GeoList.add(longitude);
        return GeoList;
        
    }
    
    
    
    
}
