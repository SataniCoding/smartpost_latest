/*Olio-ohjelmointi*/
/*Harjoitustyö*/

/*Johannes Kohvakka*/
/*0418569*/
/*Aki Hyvönen*/
/*0438400*/

import java.util.ArrayList;

/*Storage keeps a record of sent packages and packages to be sent*/
/*Also log which contains information about sent packages*/
/*getters to each attribute*/
public class Storage {
    
    private ArrayList<Package> packages = new ArrayList();
    private ArrayList<Package> sent = new ArrayList();
    static private Storage st = null;
    private String log = "";
    
    static public Storage getInstance(){
        
        if(st == null)
            st =  new Storage();

        return st;
    }
    
    
    public void addPackage(Package p){
        
        getPackages().add(p);
        
    }

    
    public ArrayList<Package> getPackages() {
        return packages;
    }
    
    public ArrayList<Package> getSent() {
        return sent;
    }
    
    
    public void sendPackage(Package p){
        
        for (int i= 0; i<packages.size();i++) {
            
            if(p == packages.get(i)) {
                sent.add(packages.get(i));
                packages.remove(i);
            }            
        }
    }
    
    //adds 
    
    public void toLog(String s){
        
        log = log.concat(s);
    }
   
    public String getLog() {
        return log;
}
    
}
