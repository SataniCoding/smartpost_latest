/*Olio-ohjelmointi*/
/*Harjoitustyö*/

/*Johannes Kohvakka*/
/*0418569*/
/*Aki Hyvönen*/
/*0438400*/


import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * 
 */
public class FXMLDocumentController implements Initializable {
    
    
    @FXML
    private WebView web;
    int nOP = 0 ;
    DataBuilder T;
    @FXML
    private ComboBox<String> cityDrop;
    @FXML
    private ComboBox<Package> packageCombo;
    Storage storage;
    @FXML
    private Button updateButton;
    @FXML
    private TextArea logArea;
    
    
    @Override
    
    
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        web.getEngine().load(getClass().getResource("index.html").toExternalForm());
        storage = null;
        storage = Storage.getInstance();
        
        //Creating Databuilder which parses data of smartposts
        try {
            T = new DataBuilder();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        ArrayList<String> cities = T.getCities();
            
        
        //Cities from parsed data to Combobox
        for (int i = 0; i<cities.size(); i++ ) {
                  
            cityDrop.getItems().add(cities.get(i));
            
        }
        //initializing the log tab
        logArea.appendText("Kirjanpito lähetetyistä paketeista:\n");
        logArea.appendText("***************************************************************\n");
    }

    @FXML
    //Draws selected city's smartposts to map
    private void pointAction(ActionEvent event) {
        String city = cityDrop.getValue();
        ArrayList<Smartpost> posts = new ArrayList();
        
        posts = T.getPosts(city);
        
        for(int i=0;i<posts.size();i++) {
            web.getEngine().executeScript("document.goToLocation("+posts.get(i).getLocation() +")");
        }
        
    }

    @FXML
    //removes drawn paths from map
    private void removeAction(ActionEvent event) {
        web.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    //when button "luo paketti" gets pushed
    //opens pop up where you can create new packages
    private void createAction(ActionEvent event) throws IOException {
        
 
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("FXMLPackageDocument.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        scene.getStylesheets().add
        (FXMLPackageController.class.getResource("newCascadeStyleSheet_1.css").toExternalForm());
        stage.show();
          
    }

    //updates packages in storage to combobox
    @FXML
    private void UpdateAction(ActionEvent event) {
        
        packageCombo.getItems().clear();
        
        ArrayList<Package> packages = new ArrayList();
        packages = storage.getPackages();
        for(int i = 0; i<packages.size();i++) {
            packageCombo.getItems().add(packages.get(i));
            
        }
        
    }

    @FXML
    
    //when "lähetä" button is pushed
    //draws path to map and writes sent package information to log
    //and updates storage-dropbox
    private void sendAction(ActionEvent event) {
        
        if(packageCombo.getValue() != null) {
            ArrayList<String> route = new ArrayList();
            ArrayList<String> gp1 = new ArrayList();
            ArrayList<String> gp2 = new ArrayList();
            
            gp1 = packageCombo.getValue().getStartPost().GeoPoint();
            gp2 = packageCombo.getValue().getDestinationPost().GeoPoint();
            
            route.add(gp1.get(0));
            route.add(gp1.get(1));
            route.add(gp2.get(0));
            route.add(gp2.get(1)); 
            
            double distance = (double)web.getEngine().executeScript("document.createPath("+ route +",'red',"+packageCombo.getValue().getType()+")");
            storage.sendPackage(packageCombo.getValue());
            
            nOP ++;
            Package p = packageCombo.getValue(); 
            
            String r1 = nOP +". " + p.contents.getName()+"    "+ p.startPost.getCity() + " -> " + p.destinationPost.getCity() + "    ("+ distance +"km)\n";
            logArea.appendText(r1);
            logArea.appendText("***************************************************************\n");
            storage.toLog(r1);
            storage.toLog("***************************************************************\n");
           
            updateButton.fire();
            
             
        
        }
            
        
    }

    @FXML
    private void logAction(Event event) {
    }
}



   

    
