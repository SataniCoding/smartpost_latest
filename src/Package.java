/*Olio-ohjelmointi*/
/*Harjoitustyö*/

/*Johannes Kohvakka*/
/*0418569*/
/*Aki Hyvönen*/
/*0438400*/


/**
 *
 * Abstract class Package contains:
 * int type; class of post package. 1, 2 or 3
 * Object contents; item in the package
 * Smartpost StartPost; from where
 * Smartpost DestinationPost; to where
 * and getters
 */

public abstract class Package {
    
    protected int type;              
    protected Object contents;
    protected Smartpost startPost;
    protected Smartpost destinationPost;
   

    public Package(int a, Object b, Smartpost c, Smartpost d){
        
        type = a;         
        contents = b;
        startPost = c;
        destinationPost = d;
        
    }
    
    public int getType() {
        return type;
    }
    
    public Smartpost getStartPost() {
        return startPost;
    }

    public Smartpost getDestinationPost() {
        return destinationPost;
    }
    
    
    @Override
    public String toString() {
       return ("Luokka "+type+", "+contents.getMass()+" kg");
    }


}




