/*Olio-ohjelmointi*/
/*Harjoitustyö*/

/*Johannes Kohvakka*/
/*0418569*/
/*Aki Hyvönen*/
/*0438400*/


import com.sun.deploy.util.StringUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * 
 */
public class FXMLPackageController implements Initializable {
    
    private Label label;
    
    @FXML
    private ComboBox<String> sCity;
    @FXML
    private ComboBox<String> dCity;
    @FXML
    private ComboBox<Smartpost> sDevice;
    @FXML
    private ComboBox<Smartpost> dDevice;
    
    DataBuilder T;
    @FXML
    private TextField nameField;
    private TextField sizeField;
    @FXML
    private TextField massField;
    @FXML
    private ComboBox<Object> objectBox;
    @FXML
    private CheckBox breakable;
    @FXML
    private RadioButton class1Button;
    @FXML
    private RadioButton class2Button;
    @FXML
    private RadioButton class3Button;
    @FXML
    private Label classLabel;
    @FXML
    private WebView testBox;
    Storage storage = null;
    @FXML
    private TextField sizeField1;
    @FXML
    private TextField sizeField2;
    @FXML
    private TextField sizeField3;
    @Override
    
    
    
    
    public void initialize(URL url, ResourceBundle rb) {
        
        //Creating sample objects
        ArrayList<Object> objectList =new ArrayList();       
        
        objectList.add(new Object("taulu", "49x30x4",4,true));
        objectList.add(new Object("kivi", "60x60x60",203,false));
        objectList.add(new Object("makkara","4x15x4", 0.1,true ));
        objectList.add(new Object("kukko","39x42x15", 3, true));
        objectList.add(new Object("sukset", "202x8x6",3,false));
        
        
        final ToggleGroup group = new ToggleGroup();
        class1Button.setToggleGroup(group);
        class2Button.setToggleGroup(group);
        class3Button.setToggleGroup(group);
        
        
        storage = Storage.getInstance();
            
        try {
            T = new DataBuilder();
        } catch (IOException | ParserConfigurationException | SAXException ex) {
            Logger.getLogger(FXMLPackageController.class.getName()).log(Level.SEVERE, null, ex);
        }                
        
        ArrayList<String> cities = T.getCities();
                    
        //Cities to combobox    
        for (int i = 0; i<cities.size(); i++ ) {

            sCity.getItems().add(cities.get(i));
            dCity.getItems().add(cities.get(i));
        }   
        
        //Sample objects to combobox
        for(int i = 0;i<objectList.size();i++) {
            objectBox.getItems().add(objectList.get(i));
        }
        
        testBox.getEngine().load(getClass().getResource("index.html").toExternalForm());
            
    }
        
        

    
    //when city is choosed search all smartposts in given city
    //and adds them to smartpost combobox
    @FXML
    private void sCityOnAction(ActionEvent event) {
        
        sDevice.getItems().clear();
        String kaupunki = sCity.getValue();
        ArrayList<Smartpost> posts = new ArrayList();
        
        posts = T.getPosts(kaupunki);
        
        for(int i=0;i<posts.size();i++) {
            sDevice.getItems().add(posts.get(i));
        }
        
    }

   
    //when city is choosed search all smartposts in given city
    //and adds them to smartpost combobox
     @FXML
    private void dCityOnAction(ActionEvent event) {
        
        dDevice.getItems().clear();
        String kaupunki = dCity.getValue();
        ArrayList<Smartpost> posts = new ArrayList();
        
        posts = T.getPosts(kaupunki);
        
        for(int i=0;i<posts.size();i++) {
            dDevice.getItems().add(posts.get(i));
        }
    }

    
    //hides current window when "peruuta" is pressed
    @FXML
    private void cancelAction(ActionEvent event) throws IOException {
        
        
        ((Node)event.getSource()).getScene().getWindow().hide();
        
    }

    
    
    
    //Creates Package when "luo paketti" is pressed
    //
    @FXML
    private void createAction(ActionEvent event) throws IOException {
        
        Object object;
        //if object is not selected or no information given to create object
        if((objectBox.getSelectionModel().isEmpty()) && (sizeField1.getText().isEmpty() 
                || sizeField2.getText().isEmpty() || sizeField3.getText().isEmpty() 
                || nameField.getText().isEmpty() || massField.getText().isEmpty())){
            classLabel.setText("Valitse esine tai luo sellainen.");
            return;           
         //if sample object is selected  
        }else if (!(objectBox.getSelectionModel().isEmpty())) {
            object = objectBox.getValue();
        } else {
            //create new object
            try {
            
            String name = nameField.getText();
            String size = (sizeField1.getText() + "x" + sizeField2.getText() + "x" + sizeField3.getText());
            double mass = Double.parseDouble(massField.getText());
            boolean breakab =breakable.isSelected();
            
            
            object = new Object(name, size, mass, breakab);
            }catch (NumberFormatException a) {
                classLabel.setText("Esineen tiedoissa häikkää!");
                return;
            }
            
        }
        //is destination and departure smartpost choosen  
        if(sDevice.getSelectionModel().isEmpty() || dDevice.getSelectionModel().isEmpty())  {
            classLabel.setText("Valitse lähtö- ja kohdeautomaatit!");
            return;
        }
       
        
        //conditions to different classes 
        
        if(class1Button.isSelected()){

            ArrayList<String> route = new ArrayList();
            ArrayList<String> gp1 = new ArrayList();
            ArrayList<String> gp2 = new ArrayList();
            gp1 = sDevice.getValue().GeoPoint();
            gp2 = dDevice.getValue().GeoPoint();
            route.add(gp1.get(0));
            route.add(gp1.get(1));
            route.add(gp2.get(0));
            route.add(gp2.get(1));
            System.out.println(route);
            double length =(double)testBox.getEngine().executeScript("document.createPath("+ route +",'red',3)");
            boolean brkble = object.getBreakable(); 
            
            
            
            if(length<150 && !(brkble) ) {
                FirstClass p = new FirstClass(object, sDevice.getValue(),dDevice.getValue());
                storage.addPackage(p);
                ((Node)event.getSource()).getScene().getWindow().hide();
            } else {
                classLabel.setText("Paketti ei täytä ensimmäisen luokan ehtoja!");
                return;
            }
            
            

        }else if(class2Button.isSelected()){
            
            int[] temp = Arrays.stream(object.getSize().split("x")).map(String::trim).mapToInt(Integer::parseInt).toArray();
            System.out.println(temp[0]+"+"+temp[1]+"+"+temp[2]);
            if(temp[0] <= 50 && temp[1]<=50 && temp[2]<=50) {
                SecondClass p = new SecondClass(object, sDevice.getValue(),dDevice.getValue());
                storage.addPackage(p);
                ((Node)event.getSource()).getScene().getWindow().hide();
            } else {
                classLabel.setText("Paketti ei täytä toisen luokan ehtoja!");
                return;
            }
            
            //secondClass

        }else if(class3Button.isSelected()){

            if(!(object.getBreakable())) {
                ThirdClass p = new ThirdClass(object, sDevice.getValue(),dDevice.getValue());
                
                storage.addPackage(p);
                ((Node)event.getSource()).getScene().getWindow().hide();
                 
            } else {
                classLabel.setText("Paketti ei täytä kolmannen luokan ehtoja!");
                return;
            }

        } else {
            classLabel.setText("Valitse pakettiluokka!");
        }
    }

    
    //gives info about different classes when infobutton is clicked
    @FXML
    private void infoAction(ActionEvent event) throws IOException {
        
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("FXMLinfo.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
        
    }

    
    //disables new object infofields when default object is chosen
    @FXML
    private void objectBoxAction(ActionEvent event) {
        
        breakable.setDisable(true);
        sizeField1.setDisable(true);
        sizeField2.setDisable(true);
        sizeField3.setDisable(true);
        massField.setDisable(true);
        nameField.setDisable(true);
        Object object = objectBox.getValue();
        int[] temp = Arrays.stream(object.getSize().split("x")).map(String::trim).mapToInt(Integer::parseInt).toArray();
        System.out.println(temp[0]+"+"+temp[1]+"+"+temp[2]);
        breakable.setSelected(objectBox.getValue().getBreakable());
        sizeField1.setText(Integer.toString(temp[0]));
        sizeField2.setText(Integer.toString(temp[1]));
        sizeField3.setText(Integer.toString(temp[2]));
        nameField.setText(objectBox.getValue().getName());
        massField.setText(String.valueOf(objectBox.getValue().getMass()));
        
            
        
    }
    
}
